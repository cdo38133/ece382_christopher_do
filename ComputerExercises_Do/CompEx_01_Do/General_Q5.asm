;-------------------------------------------------------------------------------
; CE1 - Introduction to the MSP430 and Code Composer Studio
; C2C Christopher Do, USAF / 23 Aug 2017 / 24 Aug 2017
;
; This program is a demonstration of using the CCS IDE to
; program, assemble, flash, and debug the MSP430.
;-------------------------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file

;-------------------------------------------------------------------------------
            .def    RESET                   ; Export program entry-point to
                                            ; make it known to linker.
;-------------------------------------------------------------------------------
            .text                           ; Assemble into program memory.
            .retain                         ; Override ELF conditional linking
                                            ; and retain current section.
            .retainrefs                     ; And retain any sections that have
                                            ; references to current section.

;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer


;-------------------------------------------------------------------------------
; Main loop here
;-------------------------------------------------------------------------------

			mov		#0x0200, r6 			; Set r6 to be the start of the count
			mov		#0, r7					; Set r7 to be 0, we'll use r7 to add
			mov 	#0x03ff, r8				; r8 will contain the final location
			mov 	#0, 0(r8)				; Make sure the final location is clear

loop        tst     0(r8)					; Test the final location
            jne     forever					; If it is not zero, exit loop

            mov 	r7, 0(r6)				; Else, set the current location r6 points
											; at to the current value of r7
            incd 	r6						; Double increment r6 to shift two bytes
            incd	r7						; Double increment r7 to add two

            jmp 	loop					; Continue the loop

forever     jmp     forever					; CPU Trap


;-------------------------------------------------------------------------------
; Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect   .stack

;-------------------------------------------------------------------------------
; Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET

