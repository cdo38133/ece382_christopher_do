;-------------------------------------------------------------------------------
; Assignment03 - Control Flow
; C2C Christopher Do, USAF / 29 Aug 2017 / 30 Aug 2017
;
; This program provides a solution for the problem
; posed in Assignment 3 for ECE 382.
;-------------------------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file
            
;-------------------------------------------------------------------------------
            .def    RESET                   ; Export program entry-point to
                                            ; make it known to linker.
;-------------------------------------------------------------------------------
            .text                           ; Assemble into program memory.
            .retain                         ; Override ELF conditional linking
                                            ; and retain current section.
            .retainrefs                     ; And retain any sections that have
                                            ; references to current section.

;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer


;-------------------------------------------------------------------------------
; Main loop here
;-------------------------------------------------------------------------------

			mov		&0x0216, r5				; Load the contents of 0x0216 into r5
			cmp		#0x1234, r5				; Compare #0x01234 with r5
			jl		cmp2					; If r5 was less than, go to cmp2
			jeq		cmp2					; Else if it was equal, go to cmp2
			mov		#20, r6					; Else load #20 into r6
			clr		&0x0206					; Empty &0x0206
loop1		add		r6, &0x0206				; Add r6 and &0x0206
			sub		#1, r6					; Decrement r6 by 1
			tst		r6						; Test r6
			jz 		forever					; If r6 is zero, go to CPU trap
			jmp 	loop1					; Else continue the loop
cmp2		cmp 	#0x1000, r5				; Compare #0x01000 with r5
			jl		cmp3					; If r5 was less than, go to cmp3
			jeq		cmp3					; Else if it was equal, go to cmp2
			add 	#0xeec0, r5				; Add #0xeec0 to r5
			clr		&0x0202					; Clear &0x0202
			adc		&0x0202					; Add the carry to &0x0202
			jmp		forever					; Go to CPU Trap
cmp3		bit.w	#0000000000000001,r5	; Test if the LSB is a 1
			jnc     even					; If even jump to even
			rla		r5						; Else multiply through an arithmetic shift left
			mov.w	r5, &0x0212				; Store answer in &0x0212
			jmp		forever					; Go to CPU Trap
even		rra		r5						; Divide through an arithmetic shift right
			mov.w	r5, &0x0212				; Store answer in &0x0212
			jmp		forever					; Go to CPU Trap
forever		jmp forever						; CPU Trap

;-------------------------------------------------------------------------------
; Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect   .stack
            
;-------------------------------------------------------------------------------
; Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET
            
