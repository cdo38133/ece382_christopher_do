/**********************************************************************

 COPYRIGHT 2017 United States Air Force Academy All rights reserved.

 United States Air Force Academy     __  _______ ___    _________
 Dept of Electrical &               / / / / ___//   |  / ____/   |
 Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
 USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|

 ----------------------------------------------------------------------

   FILENAME      : Assignment06.c
   AUTHOR(S)     : Christopher Do
   DATE          : 10/4/2017
   COURSE		 : ECE 382

   Assignment 6

   DESCRIPTION   : This code is a solution to Assignment 6

   DOCUMENTATION : None

 Academic Integrity Statement: I certify that, while others may have
 assisted me in brain storming, debugging and validating this program,
 the program itself is my own work. I understand that submitting code
 which is the work of other individuals is a violation of the honor
 code.  I also understand that if I knowingly give my original work to
 another individual is also a violation of the honor code.

 **********************************************************************/

#include <msp430.h> 
#include <stdint.h>	 // provides std data types (e.g., uint8_t)
#include <stdbool.h> // provides bool type for true and false

#define THRESHOLD 0x38
  /************************************************************************************/
 /* MAIN    																		 */
/************************************************************************************/
void main(void) {
	WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer

    volatile unsigned int val1 = 0x40;	// Initialize the three unsigned int values
	volatile unsigned int val2 = 0x35;
	volatile unsigned int val3 = 0x42;

	volatile unsigned char result1 = 0; // Initialize the three unsigned char values
	volatile unsigned char result2 = 0;
	volatile unsigned char result3 = 0;

	if (val1 > THRESHOLD)				// First check
	{
		volatile unsigned int fib1 = 1;
		volatile unsigned int fib2 = 1;
		volatile unsigned int temp = 0;
		volatile unsigned int count = 2;

		while (count != 10)				// Fibonacci Loop
				{
					temp = fib1 + fib2;
					fib1 = fib2;
					fib2 = temp;
					count ++;
				}
		result1 = fib2;
	}

	if (val2 > THRESHOLD)				// Second Check
	{
		result2 = 0xAF;
	}

	if (val3 > THRESHOLD)				// Third Check
		{
			result3 = val2 - 0x10;
		}

	while(true);						// CPU trap
}
