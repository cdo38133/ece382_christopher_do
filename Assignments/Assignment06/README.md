# Assignment 6 - Your First C Program

## By C2C Christopher Do

![Debugger Screenshot](debugger_screenshot.png)
##### Figure 1: Screenshot of the debugger showing functioning code.

###Assignment Questions
1. Exactly where in memory on the microcontroller are your variables stored?  
The variables are stored on the stack in RAM.
2. How do you know?  
I know this because I can look at the location of the variables in the Variables window as well as look at 0x0400 in the memory browser.  Figure 1 shows the variables are indeed in RAM.  

### Documentation
None
