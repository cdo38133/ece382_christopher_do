;-------------------------------------------------------------------------------
; MSP430 Assembler Code Template for use with TI Code Composer Studio
;
;
;-------------------------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file
            
;-------------------------------------------------------------------------------
            .def    RESET                   ; Export program entry-point to
                                            ; make it known to linker.
;-------------------------------------------------------------------------------
            .text                           ; Assemble into program memory.
            .retain                         ; Override ELF conditional linking
                                            ; and retain current section.
            .retainrefs                     ; And retain any sections that have
                                            ; references to current section.

;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer


;-------------------------------------------------------------------------------
; Main loop here
;-------------------------------------------------------------------------------
			;mov #5, r5
			;mov #6, r6
			;cmp r5, r6

			;jl less
			;jeq equal

			;mov #5, r5
			;mov #55, r6
			;cmp r5, r6

			;jl less
			;jeq equal

			;mov #6, r5
			;mov #5, r6
			;cmp r5, r6

			;jl less
			;jeq equal

			;mov #5, r5
			;mov #6, r6

			;add r5, r6
			;sub #1, r5

			mov		#10, r5
			rra		r5
			rla		r5

			mov		#5, r5					; Load the contents of 0x0216 into r5
			cmp		#4, r5					; Compare #0x01234 with r5
			jl		cmp2					; If r5 was less than, go to cmp2
			jeq		cmp2					; Else if it was equal, go to cmp2
			mov		#20, r6					; Load #20 into r6
			clr		&0x0206					; Empty &0x0206
loop1		add		r6, &0x0206				; Add r6 and &0x0206
			sub		#1, r6					; Decrement r6 by 1
			tst		r6						; Test r6
			jz forever						; If r6 is zero, CPU trap
			jmp loop1						; Else continue the loop

cmp2		jmp cmp2

			jmp forever

less		jmp less
equal		jmp equal
greater		jmp greater

forever		jmp	forever


                                            

;-------------------------------------------------------------------------------
; Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect   .stack
            
;-------------------------------------------------------------------------------
; Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET
            
