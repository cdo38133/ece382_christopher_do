/**********************************************************************

 COPYRIGHT 2017 United States Air Force Academy All rights reserved.

 United States Air Force Academy     __  _______ ___    _________
 Dept of Electrical &               / / / / ___//   |  / ____/   |
 Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
 USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|

 ----------------------------------------------------------------------

   FILENAME      : pong.c
   AUTHOR(S)     : Christopher Do
   DATE          : 10/10/2017
   COURSE		 : ECE 382

   Assignment 7

   DESCRIPTION   : A C program that implements a subset of the
   functionality of the video "pong" game. Utilizes structures
   to represent a ball's position and velocity and handles when
   the ball bounces against a wall.

   DOCUMENTATION : None

 Academic Integrity Statement: I certify that, while others may have
 assisted me in brain storming, debugging and validating this program,
 the program itself is my own work. I understand that submitting code
 which is the work of other individuals is a violation of the honor
 code.  I also understand that if I knowingly give my original work to
 another individual is also a violation of the honor code.

 **********************************************************************/

#include <msp430.h>
#include <stdint.h>	 // provides std data types (e.g., uint8_t)
#include <stdio.h>
#include <stdlib.h>
#include "pong.h"


  /************************************************************************************/
 /* FUNCTION DECLARATIONS															 */
/************************************************************************************/

/**************************************************
   Function: createBall()
   Author: Christopher Do
   Description: Creates a ball structure and returns its pointer
   Inputs: int xPos, int yPos, int xVel, int yVel, unsigned char radius
   Returns: ball_t *
***************************************************/

	ball_t * createBall(int xPos, int yPos, int xVel, int yVel, unsigned char radius)
	{
		vector2d_t * tempPosition = (vector2d_t *)malloc(sizeof(vector2d_t));
		tempPosition->x = xPos;
		tempPosition->y = yPos;

		vector2d_t * tempVelocity = (vector2d_t *)malloc(sizeof(vector2d_t));
		tempVelocity->x = xVel;
		tempVelocity->y = yVel;

		ball_t * ball = (ball_t *)malloc(sizeof(ball_t));
		ball->position = tempPosition;
		ball->velocity = tempVelocity;
		ball->radius = radius;

		return ball;
	}

/**************************************************
   Function: moveBall()
   Author: Christopher Do
   Description: "Moves" a ball structure
   Inputs: ball_t *
   Returns: None
***************************************************/

	void moveBall(ball_t * ballToMove)
	{
		ballToMove->position->x = (ballToMove->position->x) + (ballToMove->velocity->x * SPEED);
		ballToMove->position->y = (ballToMove->position->y) + (ballToMove->velocity->y * SPEED);

		if (checkCollisionX(ballToMove) == TRUE)
		{
			ballToMove->velocity->x *= -1;
		}

		if (checkCollisionY(ballToMove) == TRUE)
		{
			ballToMove->velocity->y *= -1;
		}
	}

/**************************************************
   Function: checkCollisionX()
   Author: Christopher Do
   Description: Checks for a collision on the left
   and right edges
   Inputs: ball_t *
   Returns: TRUE or FALSE
***************************************************/

	char checkCollisionX(ball_t * ballToMove)
	{
		if (ballToMove->position->x - ballToMove->radius <= 0 || ballToMove->position->x + ballToMove->radius >= SCREEN_WIDTH)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}


/**************************************************
   Function: checkCollisionY()
   Author: Christopher Do
   Description: Checks for a collision on the top
   and bottom edges
   Inputs: ball_t *
   Returns: TRUE or FALSE
***************************************************/

	char checkCollisionY(ball_t * ballToMove)
	{
		if (ballToMove->position->y - ballToMove->radius <= 0 || ballToMove->position->y + ballToMove->radius >= SCREEN_HEIGHT)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
