# Assignment 7 - Pong

## By C2C Christopher Do

![Left Wall](images/preLeft.jpg)
###### Figure 1: Screenshot before the ball collides with the left wall. Note the ball moving towards the left wall (x = 0). Radius of the ball is 5. The "speed" of the ball is 5.

![Left Wall](images/left.jpg)
###### Figure 2: Screenshot while the ball collides with the left wall. Note the change of the ball's x velocity from -1 to 1.

![Left Wall](images/postLeft.jpg)
###### Figure 3: Screenshot after the ball collides with the left wall. Note the ball moving away from the left wall.

![Right Wall](images/preRight.jpg)
###### Figure 4: Screenshot before the ball collides with the right wall. Note the ball moving towards the right wall (x = 240). Radius of the ball is 5. The "speed" of the ball is 5.

![Right Wall](images/right.jpg)
###### Figure 5: Screenshot while the ball collides with the right wall. Note the change of the ball's x velocity from 1 to -1.

![Right Wall](images/postRight.jpg)
###### Figure 6: Screenshot after the ball collides with the right wall. Note the ball moving away from the right wall.

![Top Wall](images/preTop.jpg)
###### Figure 7: Screenshot before the ball collides with the top wall. Note the ball moving towards the top wall (y = 0). Radius of the ball is 5. The "speed" of the ball is 5.

![Top Wall](images/top.jpg)
###### Figure 8: Screenshot while the ball collides with the top wall. Note the change of the ball's y velocity from -1 to 1.

![Top Wall](images/postTop.jpg)
###### Figure 9: Screenshot after the ball collides with the top wall. Note the ball moving away from the top wall.

![Bottom Wall](images/preBottom.jpg)
###### Figure 10: Screenshot before the ball collides with the bottom wall. Note the ball moving towards the bottom wall (y=320). Radius of the ball is 5. The "speed" of the ball is 5.

![Bottom Wall](images/bottom.jpg)
###### Figure 11: Screenshot while the ball collides with the bottom wall. Note the change of the ball's x velocity from 1 to -1.

![Bottom Wall](images/postBottom.jpg)
###### Figure 12: Screenshot after the ball collides with the bottom wall. Note the ball moving away from the bottom wall.

###Assignment Questions
1. How did you verify your code functions correctly?  
I initialized the ball close to the left wall and moved it towards the left and observed the changes made to the structure as it approached the wall, collided with wall, and left the wall.  I then moved the ball to the right, top, and bottom wall and repeated the same observations for each respective wall.
2. How could you make the "collision detection" helper functions only visible to your implementation file (i.e. your main.c could not call those functions directly)?   
Use a static function to limit the scope of the function to only the implementation file.

### Documentation
C2C Beresford helped with my debugging. I forgot to malloc my position and velocity vectors.

I utilized https://stackoverflow.com/questions/558122/what-is-a-static-function to read up on static functions.