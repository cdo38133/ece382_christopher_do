/**********************************************************************

 COPYRIGHT 2017 United States Air Force Academy All rights reserved.

 United States Air Force Academy     __  _______ ___    _________
 Dept of Electrical &               / / / / ___//   |  / ____/   |
 Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
 USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|

 ----------------------------------------------------------------------

   FILENAME      : pong.h
   AUTHOR(S)     : Christopher Do
   DATE          : 10/6/2017
   COURSE		 : ECE 382

   Assignment 7

   DESCRIPTION   : A C program that implements a subset of the
   functionality of the video "pong" game. Utilizes structures
   to represent a ball's position and velocity and handles when
   the ball bounces against a wall.

   DOCUMENTATION : Based off of the .h file provided by Maj Falkinburg

 Academic Integrity Statement: I certify that, while others may have
 assisted me in brain storming, debugging and validating this program,
 the program itself is my own work. I understand that submitting code
 which is the work of other individuals is a violation of the honor
 code.  I also understand that if I knowingly give my original work to
 another individual is also a violation of the honor code.

 **********************************************************************/
#ifndef _PONG_H
#define _PONG_H

#define SCREEN_WIDTH 240
#define SCREEN_HEIGHT 320
#define SPEED 5
#define TRUE ((char)1)
#define FALSE ((char)0)

typedef struct {
    int x;
    int y;
} vector2d_t;

typedef struct {
    vector2d_t * position;
    vector2d_t * velocity;
    unsigned char radius;
} ball_t;

ball_t * createBall(int xPos, int yPos, int xVel, int yVel, unsigned char radius);

void moveBall(ball_t * ballToMove);

char checkCollisionX(ball_t * ballToMove);

char checkCollisionY(ball_t * ballToMove);

#endif
