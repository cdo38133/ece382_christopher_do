/**********************************************************************

 COPYRIGHT 2017 United States Air Force Academy All rights reserved.

 United States Air Force Academy     __  _______ ___    _________
 Dept of Electrical &               / / / / ___//   |  / ____/   |
 Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
 USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|

 ----------------------------------------------------------------------

   FILENAME      : main.c
   AUTHOR(S)     : Christopher Do
   DATE          : 10/6/2017
   COURSE		 : ECE 382

   Assignment 7

   DESCRIPTION   : A C program that implements a subset of the
   functionality of the video "pong" game. Utilizes structures
   to represent a ball's position and velocity and handles when
   the ball bounces against a wall.

   DOCUMENTATION : None

 Academic Integrity Statement: I certify that, while others may have
 assisted me in brain storming, debugging and validating this program,
 the program itself is my own work. I understand that submitting code
 which is the work of other individuals is a violation of the honor
 code.  I also understand that if I knowingly give my original work to
 another individual is also a violation of the honor code.

 **********************************************************************/

#include <msp430.h> 
#include <stdint.h>	 // provides std data types (e.g., uint8_t)
#include <stdio.h>
#include <stdlib.h>
#include "pong.h"

  /************************************************************************************/
 /* MAIN    																		 */
/************************************************************************************/
void main(void)
{
	WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer
		ball_t * myBall = createBall(15, 160, -1, -1, 5); // Left Wall

		moveBall(myBall);
		moveBall(myBall);
		moveBall(myBall);

		myBall->position->x = 225; // Right Wall
		myBall->position->y = 160;
		myBall->velocity->x = 1;
		myBall->velocity->y = 1;

		moveBall(myBall);
		moveBall(myBall);
		moveBall(myBall);

		myBall->position->x = 120; // Top Wall
		myBall->position->y = 15;
		myBall->velocity->x = 1;
		myBall->velocity->y = -1;

		moveBall(myBall);
		moveBall(myBall);
		moveBall(myBall);

		myBall->position->x = 120; // Bottom Wall
		myBall->position->y = 305;
		myBall->velocity->x = -1;
		myBall->velocity->y = 1;

		moveBall(myBall);
		moveBall(myBall);
		moveBall(myBall);

		while(TRUE);						// CPU trap
}
