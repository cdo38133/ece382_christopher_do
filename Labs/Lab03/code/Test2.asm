;-------------------------------------------------------------------------------
; MSP430 Assembler Code Template for use with TI Code Composer Studio
;
;
;-------------------------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file

;-------------------------------------------------------------------------------
            .def    RESET                   ; Export program entry-point to
                                            ; make it known to linker.
;-------------------------------------------------------------------------------
            .text                           ; Assemble into program memory.
            .retain                         ; Override ELF conditional linking
                                            ; and retain current section.
            .retainrefs                     ; And retain any sections that have
                                            ; references to current section.

;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer


;-------------------------------------------------------------------------------
; Main loop here
;-------------------------------------------------------------------------------
				mov.b	#CALBC1_8MHZ, &BCSCTL1				; Setup 8MHz clock
				mov.b	#CALDCO_8MHZ, &DCOCTL

				bis.b  #BIT0, &P1DIR
here:			bic.b  #BIT0, &P1OUT
				call   #Delay160ms
				bis.b  #BIT0, &P1OUT
				call   #Delay160ms
				jmp    here

Delay160ms:
				push	r8
				push	r7

				mov.w	#0x000F,	r8

delay:
				mov.w	#0x8BC0,	r7

delayLoop:		dec		r7
				jnz		delayLoop

				dec		r8
				jnz		delay

				pop		r7
				pop		r8
				ret




;-------------------------------------------------------------------------------
; Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect   .stack

;-------------------------------------------------------------------------------
; Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET

