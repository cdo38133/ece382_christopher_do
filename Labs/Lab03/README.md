# Lab 3 - SPI - "I/O"

## By C2C Christopher Do

## Table of Contents
1. [Mega Prelab](#markdown-header-mega-prelab)
2. [Lab](#markdown-header-lab)
3. [Documentation](#markdown-header-documentation)

### Mega Prelab

I modeled my 160 ms delay subroutine after the one in Lesson 14.  In order to determine how many cycles were needed, I first had to check the period and frequency of my MSP430's 8 MHz clock to be able to correct for an inaccurate clock.  Using the logic analyzer I found the 8 MHz's period to be 0.0001 ms and frequency to be 10 MHz.  160 ms / 0.0001 ms = 1,600,000.  I determined the number of cycles for each instruction and then created an equation (found below) using those numbers equal to 1,600,000.  I let "x" be 0xF and solved for "y." The loops within my code decrements 0x8AE2 to zero, 0xF times.

5 + 3 + 3 + 2 + (x * (2 + 1 + 2 + y(1 + 2))) + 2 + 2 + 3  = 1,600,000
20 + F(5 + 3y) = 1,600,000
y = 0x8AE2  

However, when I tested my Delay160ms subroutine, I found that I had to let "y" be 0x8BC0 to get an actual delay that was close to 160 ms.  I found the new number by increasing the value until the delay was closer to 160 ms.  This difference may be due to my initial measurement of the 8 MHz clock not being as accurate as needed.  Proof that my delay is indeed very close to 160 ms is found in Figure 1. The amount of time from rising edge to rising edge must be divided by two as two calls to Delay160ms were used to generate the waveform.

![Delay Measurement](images/delay160ms_measurement.png)
##### Figure 1: Measurement of the Delay160ms subroutine.

| Name | Pin # | PX.X|
|:-:   | :-:   |:-:  |
| S1   |  5    | 1.3 |
| S2   |  8    | 2.0 |
| S3   |  9    | 2.1 |
| S4   |  10   | 2.2 |
| S5   |  11   | 2.3 |
| MOSI |  15   | 1.7 |
| CS   |  2    | 1.0 |
| DC   |  6    | 1.4 |
| MISO |  14   | 1.6 ||


|Signal|PxDIR     |PxREN          |PxOUT       |PxSEL         |PxSEL2         |
|:-:   |:-:       |:-:            |:-:         |:-:           |:-:            |
|S1    | bic 0x08 | bis 0x08      | bis 0x08   | bic 0x08 N/A | bic 0x08 N/A  |
|MOSI  |N/A       | bic 0x80 N/A  | bis 0x80   | bis 0x80     | bis 0x80      |
|CS    | bis 0x01 | bic 0x01      | bis 0x01   | bic 0x01 N/A | bic 0x01 N/A  ||

CS PxOUT is bic when activated.  

| Name | Pin # | Function           |
|:-:   |:-:    |:-:                 |
| SCLK |   7   | serial clock       |
| CS   |   2   | chip select        |
| MOSI |   15  | master out slave in|
| DC   |   6   | data or command    ||

| Pin name | Function | 
|:-:       |:-:       |
| P1.5     | UCB0CLK  |
| P1.7     | UCB0SIMO |
| P1.6     | UCB0SOMI ||

Line 1: Setting the UCSWRST bit in the CTL1 register resets the subsystem into a known state until it is cleared.  
Line 2: Moves the clock phase select, MSB first select, master mode select, and the synchronous mode enable bits into the CTL0 register.  
Line 3: The UCSSEL_2 setting for the UCB0CTL1 register has been chosen, selecting the SMCLK (sub-main clock) as the bit rate source clock for when the MSP 430 is in master mode.  
Line 4: Sets bit 0 of the USCI_B0 bit rate control register 0.  
Line 5: Clears USCI_B0 bitrate control register 1.  
Line 6: Sets the P1SEL bit of pins 1.5, 1.7, and 1.6.  
Line 7: Sets the P2SEL bit of pins 1.5, 1.7, and 1.6.  
Line 8: Clearing the UCSWRST bit.  

![Time Diagrams](images/timeDiagrams.jpg)
##### Figure 2: Time diagrams for writeCommand and writeData.

|Subroutine   | Purpose                                           |
|:-:          |:-:                                                | 
|setArea      | defines the area you are about to write to for LCD|
|splitColor   | splits the color word into color bytes            |
|writeCommand | send a command to the LCD                         |
|writeData    | send data to the LCD                              ||


### Lab
After the Delay160ms subroutine was implemented, a logic analyzer was used to see what packets were being sent by setArea when the s1 button was pressed using the provided code.  The table below contains information about these packets. Figure 3 displays an overall look at the waveform.  Figure 4 displays the waveform of the column address set command packet.  Figure 5 displays the waveform of the start column MSB data packet.  The values captured make sense as they represent the commands parameters being used with setArea to draw a line on the LCD. Figure 6 displays the flowchart for setArea.

|Packet|Line|Command/Data|8-bit packet|Meaning of packet |
|:-:   |:-: |:-:         |:-:         |:-:               |
|1     |404 |Command     |0x2A        |Column address set|
|2     |407 |Data        |0x00        |Start Column MSB  |
|3     |410 |Data        |0x0D        |Start Column Other|
|4     |414 |Data        |0x00        |End Column MSB    |
|5     |417 |Data        |0x00        |End Column Other  |
|6     |420 |Command     |0x12        |Page address set  |
|7     |424 |Data        |0x2B        |Start Page MSB    |
|8     |427 |Data        |0x00        |Start Page Other  |
|9     |432 |Data        |0x03        |End Page MSB      |
|10    |435 |Data        |0x00        |End Page Other    |
|11    |438 |Command     |0x2C        |Memory Write      ||

![Overall Waveform](images/overall_waveform.png)
##### Figure 3: Overall Waveform of setArea. Green markers are commands and yellow markers are data. The pink markers indicate the hex value of the 8-bit packet.  The packets are in the same order as the above table.

![Command Waveform](images/command_waveform.png)
##### Figure 4: Waveform for the column address set command packet of setArea.

![Data Waveform](images/data_waveform.png)
##### Figure 5: Waveform for the start column MSB data packet of setArea.

![Flowchart](images/flowchart.JPG)
##### Figure 6: Flowchart for setArea.

### Documentation
C2C Beresford helped me with understanding where to find the information for the LCD BoosterPack and MSP430 tables. He also explained what Line 2, 6, and 7's notations mean for the MSP430 section. I consulted with him on how he was going to handle the cycles of the time diagrams.  He also helped me figure out how to set up the logic analyzer.  C2Cs Beresford and Ramseyer assisted in helping check the logic of my plan before coding and looking at my code when I failed to miss simple mistakes in the debugging proces.
