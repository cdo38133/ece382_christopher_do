Set-up

set P1.6 as output
set P1.7 as input
set TA0.1 as the PWM signal

enable interrupts for port 1 		// for the echo detection. subject to change



Use

Servo
move left by sending 1 ms pulse		// duty cycle using TA0.1
move center by sending 1.5 ms pulse
move right by sending 2 ms pulse

Sensor
Send the ~10uS signal to the trigger
Measure the echo or time out		// Either interrupts or polling.  Will ask for advice