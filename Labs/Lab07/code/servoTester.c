/**********************************************************************

 COPYRIGHT 2017 United States Air Force Academy All rights reserved.

 United States Air Force Academy     __  _______ ___    _________
 Dept of Electrical &               / / / / ___//   |  / ____/   |
 Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
 USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|

 ----------------------------------------------------------------------

   FILENAME      : servoTester.c
   AUTHOR(S)     : Christopher Do
   DATE          : November 2017
   COURSE		 : ECE 382

   Lab 7

   DESCRIPTION   : Functionality to use a potentiometer to control
   the servo position using ADC.

   QUESTIONS:
	1) What voltage range did you select for your potentiometer analog
	servo position selector input and why?

	I'm giving the potentiometer ~3.5 volts because that is what my
	3.3 V regulator outputs.  My potentiometer therefore outputs a
	range of 0 - ~3.5 V.  I selected this in an attempt to avoid
	frying my MSP430 while still having a relatively large window
	to be able to select distinct levels easily.

	2) What values did you choose for VR+ and VR-.

	I am not really sure what this is asking.  I assume it is the max
	and min voltages being used to check for different levels. I use
	ADC10MEM to determine what TA0CCR1 value I should use. Any values
	of ADC10MEM greater than 750 result in the smallest TA0CCR1 value.
	Any values of ADC10MEM less than 50 result in the great TA0CCR1
	value.  This roughly correlates with any voltage greater than ~3.5
	and anything less than ~0.1.

	3) What calculations did you use to translate your input reference
	voltages to the different PWM values for the servo positions?

	Through experimentation I found that an
	ADC10MEM of less than 50 is essentially turning the potentiometer
	all the way counterclockwise.  An ADC10MEM of greater than 750 is
	essentially turning the potentiometer all the way clockwise. I
	decided that 11 positions would a sufficient amount of positions
	to demonstrate that my ADC was functioning.  I determined what delta
	V was needed to have 11 levels between 50 and 750.  (750 - 50) / 10
	gives a value of 70. My 11 levels of ADC10MEM therefore range from 50
	to 750 with increments of 70.  I found which values of TA0CCR1 resulted
	in servo positions of all the way left and all the way right, and then
	I did a similar process as I did for ADC10MEM values and determined
	that my 11 values of TA0CCR1 would range from 400 to 2300 with
	increments of 190.  I then did a simple association with the different
	values of ADC10MEM and TA0CCR1 and the rest is history.

	Since I wasn't supposed to use levels like I did I then used the values
	I found and plotted them on a graph in excel. I got a linear regression
	equation from Excel.  That equation is y = -2.7143x + 2435.7 where x is the
	value of ADC10MEM and y is the value that I should use for TA0CCR1.

   DOCUMENTATION : None

 Academic Integrity Statement: I certify that, while others may have
 assisted me in brain storming, debugging and validating this program,
 the program itself is my own work. I understand that submitting code
 which is the work of other individuals is a violation of the honor
 code.  I also understand that if I knowingly give my original work to
 another individual is also a violation of the honor code.

 **********************************************************************/

#include <msp430.h>
#include <stdint.h>	 // provides std data types (e.g., uint8_t)
#include <stdio.h>
#include <stdlib.h>

#define SERVO_DELAY		50000

void main(void)
{
	WDTCTL = WDTPW + WDTHOLD;                 // Stop WDT
	BCSCTL1 = CALBC1_8MHZ;
	DCOCTL = CALDCO_8MHZ;

	ADC10CTL0 = ADC10SHT_3 + ADC10ON + ADC10IE; // ADC10ON, interrupt enabled
	ADC10CTL1 = INCH_7;                       // input A7
	ADC10AE0 |= BIT7;                         // P1.7 ADC Analog Enable
	ADC10CTL1 |= ADC10SSEL1|ADC10SSEL0;                // Select SMCLK
	P1DIR |= BIT5;                            // Set P1.5 to output direction

	P1DIR |=  BIT2;					// Timer 0.1 on P1.2
	P1SEL |=  BIT2;
	P1OUT &= ~BIT2;

    TA0CTL |= TASSEL_2|MC_1|ID_3;

    TA0CCR0 = 20000;
    TA0CCR1 = 2350;
    TA0CCTL1 |= OUTMOD_7;

	TA0R = 0;						// ensure TAR is clear
	TA0CTL &= ~TAIFG;				// clear flag before enabling interrupts = good practice

	__delay_cycles(SERVO_DELAY);

	while(1)
	{
		ADC10CTL0 |= ENC + ADC10SC;             // Sampling and conversion start
		__bis_SR_register(CPUOFF + GIE);        // LPM0, ADC10_ISR will force exit

		TA0CCR1 = -2.7143 * ADC10MEM + 2435.7;

		__delay_cycles(SERVO_DELAY);
	}
}

// ADC10 interrupt service routine
#pragma vector=ADC10_VECTOR
__interrupt void ADC10_ISR(void)
{
	__bic_SR_register_on_exit(CPUOFF);        // Clear CPUOFF bit from 0(SR)
}
