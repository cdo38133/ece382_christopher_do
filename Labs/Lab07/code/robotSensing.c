/**********************************************************************

 COPYRIGHT 2017 United States Air Force Academy All rights reserved.

 United States Air Force Academy     __  _______ ___    _________
 Dept of Electrical &               / / / / ___//   |  / ____/   |
 Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
 USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|

 ----------------------------------------------------------------------

   FILENAME      : robotSensing.c
   AUTHOR(S)     : Christopher Do
   DATE          : November 2017
   COURSE		 : ECE 382

   Lab 7

   DESCRIPTION   : Functionality for a robot that can sense.

   DOCUMENTATION : None

 Academic Integrity Statement: I certify that, while others may have
 assisted me in brain storming, debugging and validating this program,
 the program itself is my own work. I understand that submitting code
 which is the work of other individuals is a violation of the honor
 code.  I also understand that if I knowingly give my original work to
 another individual is also a violation of the honor code.

 **********************************************************************/

#include <msp430.h>
#include <stdint.h>	 // provides std data types (e.g., uint8_t)
#include <stdio.h>
#include <stdlib.h>
#include "ir.h"

#define SERVO_LEFT		0
#define	SERVO_MIDDLE	1
#define	SERVO_RIGHT		2

#define SENSOR_DELAY	100
#define SERVO_DELAY		1500000
#define SIXTY_MS		60000

#define SMALL_DELAY		1250000
#define MED_DELAY		2500000
#define BIG_DELAY		10000000

#define THRESHOLD		2900

#define ECHO_PIN		(P1IN & BIT4)
#define	HIGH_2_LOW_ECHO	P1IES |= BIT4
#define	LOW_2_HIGH_ECHO	P1IES &= ~BIT4

volatile int16_t	pulseDuration = 0;
volatile uint8_t	servoPosition = SERVO_MIDDLE;

volatile uint32_t	irPacket = 0;
volatile uint16_t	packetData[32];
volatile uint8_t	newIrPacket = FALSE;
volatile uint8_t	packetIndex = 0;

void sensorTrigger();

void servoLeft();
void servoMiddle();
void servoRight();

void main(void)
{
	initMSP430();

	__delay_cycles(BIG_DELAY);

	while(1)  {
			if (newIrPacket == TRUE) {
				newIrPacket = FALSE;
				if (irPacket == OK)
				{
					servoMiddle();
					__delay_cycles(MED_DELAY);
					sensorTrigger();
					__delay_cycles(BIG_DELAY);
				}
				else if (irPacket == VOLPLUS)
				{
					servoLeft();
					__delay_cycles(MED_DELAY);
					sensorTrigger();
					__delay_cycles(BIG_DELAY);
				}
				else if (irPacket == CHPLUS)
				{
					servoRight();
					__delay_cycles(MED_DELAY);
					sensorTrigger();
					__delay_cycles(BIG_DELAY);
				}
				irPacket = 0;
			}
		}

}


/************************************************************************************/
/* FUNCTION DECLARATIONS															*/
/************************************************************************************/

/**************************************************
   Function: initMSP430()
   Author: Christopher Do
   Description: Initializes the MSP430
   Inputs: None
   Returns: None
***************************************************/
void initMSP430() {
		WDTCTL = WDTPW|WDTHOLD;			// stop the watchdog timer

	    BCSCTL1 = CALBC1_8MHZ;
	    DCOCTL = CALDCO_8MHZ;

	    P1DIR |=  BIT0;					// LEDs
	    P1OUT &= ~BIT0;
	    P1DIR |=  BIT6;
	    P1OUT &= ~BIT6;

	    P1DIR |=  BIT3;					// Trigger
	    P1SEL &= ~BIT3;
	    P1SEL2 &= ~BIT3;
	    P1OUT &= ~BIT3;

		P1DIR &= ~BIT4;					// Echo input
		P1SEL &= ~BIT4;
		P1SEL2 &= ~BIT4;
		P1OUT &= ~BIT4;

		P1DIR |=  BIT2;					// Timer 0.1 on P1.2
		P1SEL &= ~BIT2;
		P1OUT &= ~BIT2;

		P2DIR &= ~BIT6;					// IR Input
		P2SEL &= ~BIT6;
		P2SEL2 &= ~BIT6;

	    TA0CTL |= TASSEL_2|MC_1|ID_3;

	    TA0CCR0 = 20000;
	    TA0CCR1 = 1500;
	    TA0CCTL1 |= OUTMOD_7;

		P1IFG &= ~BIT4;					// Clear any interrupt flag on P1.4
		P1IE  |= BIT4;					// Enable P1.4 interrupt

		P2IFG &= ~BIT6;					// Clear any interrupt flag on P2.6
		P2IE  |= BIT6;					// Enable P2.6 interrupt

		HIGH_2_LOW;						// check the header out.  P2IES changed.
		HIGH_2_LOW_ECHO;				// check the header out.  P1IES changed.

		TA0R = 0;						// ensure TAR is clear
		TA0CTL &= ~TAIFG;				// clear flag before enabling interrupts = good practice

		TA1R = 0;						// ensure TAR is clear
		TA1CCR0 = 16000 - 1;			// create a 16ms roll-over period
		TA1CTL &= ~TAIFG;				// clear flag before enabling interrupts = good practice
		TA1CTL = ID_3 | TASSEL_2;		// Use 1:8 prescalar off SMCLK and enable interrupts

		__enable_interrupt();
}

/**************************************************
   Function: sensorTrigger()
   Author: Christopher Do
   Description: Send a pulse to trigger the ultra-
   sonic sensor.
   Inputs: None
   Returns: None
***************************************************/
void sensorTrigger()
{
	pulseDuration = 0;

	P1OUT |=  BIT3;
	__delay_cycles(SENSOR_DELAY);
	P1OUT &= ~BIT3;

	while (pulseDuration == 0);

	if (pulseDuration == -1) // If the timer over flowed
	{
		pulseDuration = THRESHOLD + 1;
	}

	if (pulseDuration < THRESHOLD && servoPosition == SERVO_LEFT)
	{
		P1OUT |=  BIT0;

		__delay_cycles(MED_DELAY);

		P1OUT &= ~BIT0;
	}

	if (pulseDuration < THRESHOLD && servoPosition == SERVO_MIDDLE)
	{
		P1OUT |=  BIT0;
		P1OUT |=  BIT6;

		__delay_cycles(MED_DELAY);

		P1OUT &= ~BIT0;
		P1OUT &= ~BIT6;
	}

	if (pulseDuration < THRESHOLD && servoPosition == SERVO_RIGHT)
	{
		P1OUT |=  BIT6;

		__delay_cycles(MED_DELAY);

		P1OUT &= ~BIT6;
	}
}


/**************************************************
   Function: servoLeft()
   Author: Christopher Do
   Description: Moves the servo towards the left
   Inputs: None
   Returns: None
***************************************************/
void servoLeft()
{
	TA0CCR1 = 2350;

	P1SEL |=  BIT2;
	__delay_cycles(SERVO_DELAY);
	P1SEL &= ~BIT2;

	servoPosition = SERVO_LEFT;
}

/**************************************************
   Function: servoMiddle()
   Author: Christopher Do
   Description: Moves the servo towards the middle
   Inputs: None
   Returns: None
***************************************************/
void servoMiddle()
{
	TA0CCR1 = 1350;

	P1SEL |=  BIT2;
	__delay_cycles(SERVO_DELAY);
	P1SEL &= ~BIT2;

	servoPosition = SERVO_MIDDLE;
}


/**************************************************
   Function: servoRight()
   Author: Christopher Do
   Description: Moves the servo towards the right
   Inputs: None
   Returns: None
***************************************************/
void servoRight()
{
	TA0CCR1 = 400;

	P1SEL |=  BIT2;
	__delay_cycles(SERVO_DELAY);
	P1SEL &= ~BIT2;

	servoPosition = SERVO_RIGHT;
}

// -----------------------------------------------------------------------
// Interrupts
// -----------------------------------------------------------------------

#pragma vector = PORT1_VECTOR

__interrupt void pinChangeEcho (void) {
	int8	echo_pin;

	if(ECHO_PIN) echo_pin = 1; else echo_pin = 0;

	switch (echo_pin) {
		case 0:
			pulseDuration = TA0R;
			TA0CTL &= ~(TAIE);
			LOW_2_HIGH_ECHO; 						// Set up pin interrupt on positive edge
			break;

		case 1:
			TA0CTL &= ~TAIFG;
			TA0CTL |= TAIE; 					// Enable timer A
			TA0R = 0x0000;						// time measurements are based at time 0
			HIGH_2_LOW_ECHO; 						// Set up pin interrupt on falling edge
			break;
	}	// end switch
	P1IFG &= ~BIT4;								// Clear the interrupt flag
} // end pinChange ISR

#pragma vector = TIMER0_A1_VECTOR
__interrupt void timerOverflowEcho (void) {
	TA0CTL &= ~TAIE;
	TA0CTL &= ~TAIFG;
	P1IFG &= ~BIT4;
	pulseDuration = -1;
} // end TimerA ISR



#pragma vector = PORT2_VECTOR			// This is from the MSP430G2553.h file

__interrupt void pinChange (void) {
	int16	pulseDuration;				// The timer is 16-bits
	int8	ir_pin;

	if(IR_PIN) ir_pin = 1; else ir_pin = 0;

	switch (ir_pin) {
		case 0:
		// !!!!!!!!!NEGATIVE EDGE!!!!!!!!!!
			pulseDuration = TA1R;			//**Note** If you don't specify TA1 or TA0 then TAR defaults to TA0R

			if (pulseDuration > minLogic0Pulse && pulseDuration < maxLogic0Pulse){			// Logic 0
				packetData[packetIndex] = pulseDuration;
				irPacket = irPacket << 1;
				packetIndex += 1;
			}
			else if (pulseDuration > minLogic1Pulse && pulseDuration < maxLogic1Pulse){		// Logic 1
				packetData[packetIndex] = pulseDuration;
				irPacket = irPacket << 1;
				irPacket += 1;
				packetIndex += 1;
			}

			TA1CTL &= ~(MC_1|TAIE); 					// Disable timer A
			LOW_2_HIGH; 						// Set up pin interrupt on positive edge
			break;

		case 1:
		// !!!!!!!!POSITIVE EDGE!!!!!!!!!!!
			TA1R = 0x0000;						// time measurements are based at time 0
			TA1CTL |= MC_1|TAIE; 					// Enable timer A
			HIGH_2_LOW; 						// Set up pin interrupt on falling edge
			break;
	}	// end switch

	P2IFG &= ~BIT6;							// Clear the interrupt flag to prevent immediate ISR re-entry

} // end pinChange ISR

#pragma vector = TIMER1_A1_VECTOR			// This is from the MSP430G2553.h file
__interrupt void timerOverflow (void) {
	TA1CTL &= ~(MC_1|TAIE); 						// Disable timer A
	newIrPacket = TRUE;
	packetIndex = 0;
	TA1CTL &= ~TAIFG;
} // end TimerA ISR
