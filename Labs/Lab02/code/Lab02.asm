;-------------------------------------------------------------------------------
; Lab 2 - Subroutines - "Cryptography"
; C2C Christopher Do, USAF / Sept 2017
;
; This program provides a solution for the problem
; posed in Lab 2 for ECE 382 by...
;-------------------------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file

;-------------------------------------------------------------------------------
            .def    RESET                   ; Export program entry-point to
                                            ; make it known to linker.
;-------------------------------------------------------------------------------
            .text                           ; Assemble into program memory
            .retain                         ; Override ELF conditional linking
                                            ; and retain current section
            .retainrefs                     ; Additionally retain any sections
                                            ; that have references to current
                                            ; section

; Required Functionality
encryptedMessage:	.byte	0xef,0xc3,0xc2,0xcb,0xde,0xcd,0xd8,0xd9,0xc0,0xcd,0xd8,0xc5,0xc3,0xc2,0xdf,0x8d,0x8c,0x8c,0xf5,0xc3,0xd9,0x8c,0xc8,0xc9,0xcf,0xde,0xd5,0xdc,0xd8,0xc9,0xc8,0x8c,0xd8,0xc4,0xc9,0x8c,0xe9,0xef,0xe9,0x9f,0x94,0x9e,0x8c,0xc4,0xc5,0xc8,0xc8,0xc9,0xc2,0x8c,0xc1,0xc9,0xdf,0xdf,0xcd,0xcb,0xc9,0x8c,0xcd,0xc2,0xc8,0x8c,0xcd,0xcf,0xc4,0xc5,0xc9,0xda,0xc9,0xc8,0x8c,0xde,0xc9,0xdd,0xd9,0xc5,0xde,0xc9,0xc8,0x8c,0xca,0xd9,0xc2,0xcf,0xd8,0xc5,0xc3,0xc2,0xcd,0xc0,0xc5,0xd8,0xd5,0x8f
messageEnd:			.byte	0xff
key:				.byte	0xac
keyEnd:				.byte	0xff

; B Functionality
;encryptedMessage:	.byte	0xf8,0xb7,0x46,0x8c,0xb2,0x46,0xdf,0xac,0x42,0xcb,0xba,0x03,0xc7,0xba,0x5a,0x8c,0xb3,0x46,0xc2,0xb8,0x57,0xc4,0xff,0x4a,0xdf,0xff,0x12,0x9a,0xff,0x41,0xc5,0xab,0x50,0x82,0xff,0x03,0xe5,0xab,0x03,0xc3,0xb1,0x4f,0xd5,0xff,0x40,0xc3,0xb1,0x57,0xcd,0xb6,0x4d,0xdf,0xff,0x4f,0xc9,0xab,0x57,0xc9,0xad,0x50,0x80,0xff,0x53,0xc9,0xad,0x4a,0xc3,0xbb,0x50,0x80,0xff,0x42,0xc2,0xbb,0x03,0xdf,0xaf,0x42,0xcf,0xba,0x50
;messageEnd:			.byte	0xff
;key:				.byte	0xac, 0xdf, 0x23
;keyEnd:				.byte	0xff

; Encrypting and Decrypting with the same key
;encryptedMessage:	.byte	0x48, 0x65, 0x6c, 0x6c, 0x6f, 0x2c, 0x20, 0x6d, 0x79, 0x20, 0x6e, 0x61, 0x6d, 0x65, 0x20, 0x69, 0x73, 0x20, 0x43, 0x68, 0x72, 0x69, 0x73, 0x74, 0x6f, 0x70, 0x68, 0x65, 0x72, 0x20, 0x44, 0x6f, 0x2e, 0x20, 0x49, 0x20, 0x6c, 0x69, 0x6b, 0x65, 0x20, 0x74, 0x6f, 0x20, 0x70, 0x6c, 0x61, 0x79, 0x20, 0x52, 0x75, 0x6e, 0x65, 0x53, 0x63, 0x61, 0x70, 0x65, 0x2e
;messageEnd:			.byte	0xff
;key:				.byte	0x59, 0x70, 0x39, 0x36
;keyEnd:				.byte	0xff

;encryptedMessage:	.byte	0x11, 0x15, 0x55, 0x5A, 0x36, 0x5C, 0x19, 0x5B, 0x20, 0x50, 0x57, 0x57, 0x34, 0x15, 0x19, 0x5F, 0x2A, 0x50, 0x7A, 0x5E, 0x2B, 0x19, 0x4A, 0x42, 0x36, 0x00, 0x51, 0x53, 0x2B, 0x50, 0x7D, 0x59, 0x77, 0x50, 0x70, 0x16, 0x35, 0x19, 0x52, 0x53, 0x79, 0x04, 0x56, 0x16, 0x29, 0x1C, 0x58, 0x4F, 0x79, 0x22, 0x4C, 0x58, 0x3C, 0x23, 0x5A, 0x57, 0x29, 0x15, 0x17
;messageEnd:			.byte	0xff
;key:				.byte	0x59, 0x70, 0x39, 0x36
;keyEnd:				.byte	0xff

; Encrypting the decrypted message with the encrypted to get the key
;encryptedMessage:	.byte	0x54, 0x68, 0x69, 0x73, 0x20, 0x69, 0x73, 0x20, 0x61, 0x20, 0x74, 0x65, 0x73, 0x74, 0x2e
;messageEnd:			.byte	0xff

;key:				.byte	0x59, 0x70, 0x39, 0x36
;keyEnd:				.byte	0xff

;key:				.byte	0x0D, 0x18, 0x50, 0x45, 0x79, 0x19, 0x4A, 0x16, 0x38, 0x50, 0x4D, 0x53, 0x2A, 0x04, 0x17
;keyEnd:				.byte	0xff

;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer

;-------------------------------------------------------------------------------
                                            ; Main loop here
;-------------------------------------------------------------------------------
			.data
decryptedMessage:	.space 100

			.text

			clr r12
			clr r13
			clr r14
			clr r15

            mov		#encryptedMessage,	r12
            mov		#key,				r13
            mov		#decryptedMessage,	r14

            mov		#keyEnd,			r6

            call    #decryptMessage

CPUtrap:    jmp     CPUtrap

;-------------------------------------------------------------------------------
                                            ; Subroutines
;-------------------------------------------------------------------------------

;-------------------------------------------------------------------------------
; Subroutine Name: decryptMessage
; Author:
; Function: Decrypts a string of bytes and stores the result in memory.  Accepts
;            the address of the encrypted message, address of the key, and address
;            of the decrypted message (pass-by-reference).  Accepts the length of
;            the message by value.  Uses the decryptCharacter subroutine to decrypt
;            each byte of the message.  Stores the results to the decrypted message
;            location.
; Inputs: message pointer in r12, key pointer in r13, write pointer in r14
; Outputs: None
; Registers destroyed: r15 destroyed in call to decryptCharacter
;-------------------------------------------------------------------------------

decryptMessage:
			cmp		#messageEnd,		r12
			jeq		exitDM

			push 	r14
			clr 	r14
			mov.b	@r12,				r14
			mov.b	@r13,				r15

			call	#decryptCharacter
			pop		r14
			mov.b	r15,				0(r14)

			inc		r12
			inc		r13
			inc		r14

			mov		#keyEnd,			r7
			cmp		#keyEnd,			r13
			jne		decryptMessage

			clr		r13
			mov		#key,				r13
			jmp		decryptMessage

exitDM:
			mov		#encryptedMessage,	r12
            mov		#key,				r13
            mov		#decryptedMessage,	r14
            ret


;-------------------------------------------------------------------------------
; Subroutine Name: decryptCharacter
; Author:
; Function: Decrypts a byte of data by XORing it with a key byte.  Returns the
;            decrypted byte in the same register the encrypted byte was passed in.
;            Expects both the encrypted data and key to be passed by value.
; Inputs: encrypted byte in r14, key byte in r15
; Outputs: result in r15
; Registers destroyed: r15
;-------------------------------------------------------------------------------

decryptCharacter:
			mov		r14,				r5
			xor.b	r14,				r15
            ret

;-------------------------------------------------------------------------------
;           Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect    .stack

;-------------------------------------------------------------------------------
;           Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET
