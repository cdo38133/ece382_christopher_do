# Lab 2 - Subroutines - "Cryptography"

## By C2C Christopher Do

## Table of Contents
1. [Changes](#markdown-header-changes)
2. [Documentation](#markdown-header-documentation)


### Changes 
For my decryptCharacter plan, I simply changed which registers were being used. For my decryptMessage plan, I changed which registers were being used and refined the idea of how I would use registers and the stack to actually process the encrypted message. The overall concept remains the same, but I made the plan more useful. My revised pseudocode is located in the "Report" folder.

I did not implement any input validation, so that is now a potential limitation of my solution.

### Documentation
None
