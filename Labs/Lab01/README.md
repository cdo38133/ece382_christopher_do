# Lab 1 - Assembly Language - "A Simple Calculator"

## By C2C Christopher Do

## Table of Contents
1. [Purpose](#markdown-header-purpose)
2. [Preliminary Design](#markdown-header-preliminary-design)
 * [Code](#markdown-header-code)
3. [Software flow chart and algorithms](#markdown-header-software-flow-chart-and-algorithms)
 * [Pseudocode](#markdown-header-pseudocode)
4. [Hardware schematic](#markdown-header-hardware-schematic)
5. [Well-formatted code](#markdown-header-well-formatted-code)
6. [Debugging](#markdown-header-debugging)
7. [Testing methodology and results](#markdown-header-testing-methodology-and-results)
8. [Answers to Lab Questions](#markdown-header-answers-to-lab-questions)
9. [Observations and Conclusions](#markdown-header-observations-and-conclusions)
10. [Feedback](#markdown-header-feedback)
11. [Documentation](#markdown-header-documentation)

### Purpose 
I will write my first complete assembly language program using what I have learned in class. I will need all of the skills I have learned to this point - the instruction set, addressing modes, conditional jumps, status register flags, assembler directives, the assembly process, etc. This lab will give me practice in using assembly to implement higher-level if/then/else and looping constructs.

### Preliminary design
I will read all of the instructions, and begin working on the pre-lab.  I will consider what the end goals of the lab are and plan on how to achieve those goals. Pseudo-code and a preliminary flowchart will be important tools to prepare for actually coding the lab. Once I have a good plan established, I will implement functionality and test as I go. Once completed, I will use the test cases to ensure that everything is functioning as expected.

#### Code:
Nothing significant to add here at this time.

### Software flow chart and algorithms
![preliminarylowChart](images/flowChart.jpg)
##### Figure 1: This is the preliminary flow chart.

![finalFlowChart](images/finalFlowChart.jpg)
##### Figure 2: This is the final flow chart.  The final plan was significantly more complex and durable than the preliminary plan.  I added in more checks on operations and operands to ensure the program would run as expected.  The initial read and checks are more robust (i.e. if end or clr are used in the first two bytes, they will be handled properly), and there are checks after results are stored.

![mulFlowChart](images/mulFlowChart.jpg)
##### Figure 3: This is the multiplication flow chart.

#### Pseudocode:
Nothing significant to add here at this time.

### Hardware schematic
Nothing significant to add here at this time.

### Well-formatted code
Nothing significant to add here at this time.

### Debugging
I had a ton of issues dealing primarily with syntax and unfamiliarity with CCS and MSP430 assembly.  To begin with, I was missing colons in labels which lead to the program building and silently failing.  I had no clue what was going on for a while and went through the unnecessary hassle of using old commits and recreating files.  While utilizing the test cases, I had to step through my program to figure out where I had various syntax errors or referenced the incorrect registers.  Incremental development definitely helped with narrowing down where errors may be occurring.

### Testing methodology and results
I will use both test cases provided as well as my own test cases to ensure that the functionality is performing as expected. To determine if the correct results are being saved, I will look at the RAM after each test case. The results saved in RAM should be in the correct spot and match the expected values for the test cases.  Also, I will step through the program several times to make sure operations are being completed as planned.  I deviated from my test plan by using a separate testCode.asm file to test individual functions such as flushing out the MUL_OP. This allowed me to more quickly test the functionality and more easily see issues.

#### Test Cases
Required Functionality  
0x11, 0x11, 0x11, 0x11, 0x11, 0x44, 0x22, 0x22, 0x22, 0x11, 0xCC, 0x55  
Result: 0x22, 0x33, 0x00, 0x00, 0xCC

B Functionality  
0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0xDD, 0x44, 0x08, 0x22, 0x09, 0x44, 0xFF, 0x22, 0xFD, 0x55  
Result: 0x22, 0x33, 0x44, 0xFF, 0x00, 0x00, 0x00, 0x02

A Functionality  
0x22, 0x11, 0x22, 0x22, 0x33, 0x33, 0x08, 0x44, 0x08, 0x22, 0x09, 0x44, 0xff, 0x11, 0xff, 0x44, 0xcc, 0x33, 0x02, 0x33, 0x00, 0x44, 0x33, 0x33, 0x08, 0x55  
Result: 0x44, 0x11, 0x88, 0x00, 0x00, 0x00, 0xff, 0x00, 0xff, 0x00, 0x00, 0xff

Additional Test 1: This tests using CLR_OP as the first instruction followed shortly by an invalid operation to make sure both are handled properly  
0x44, 0x11, 0x11, 0x22, 0x22, 0x11, 0x33, 0x05, 0x42, 0x26, 0x55  
Result: 0x00, 0x33, 0x22, 0xAA, ***invalid\_op***

Additional Test 2: This tests using CLR_OP as the second instruction to ensure it is handled properly.  It then demonstrates that if 0x55 or 0x44 (END_OP or CLR_OP) are used as second operands, they will function as operands, which may or may not be desired. I decided to not check for END_OP/CLR_OP in the second operand spot (other than in the initial checks) as their position is indicative of an operand, not an operation.  Finally, this test case demonstrates that instructions past an END_OP will not be followed.  
0x11, 0x44, 0x00, 0x11, 0x02, 0x33, 0x55, 0x11, 0x44, 0x55, 0x11, 0x11  
Result: 0x00, 0x02, 0xAA, 0xEE, ***end\_op***

#### Results
![reqFunct](images/reqFunct.jpg)
##### Figure 4: This is a screenshot after executing the Required Functionality test case.

![bFunct](images/bFunct.jpg)
##### Figure 5: This is a screenshot after executing the B Functionality test case.

![aFunct](images/aFunct.jpg)
##### Figure 6: This is a screenshot after executing the A Functionality test case.

![test1](images/test1.jpg)
##### Figure 7: This is a screenshot after executing the first additional test case.

![test2](images/test2.jpg)
##### Figure 8: This is a screenshot after executing the second test case.

### Answers to Lab Questions
#####1. How will you test your code? Be specific on how you will verify correctness of results with a repeatable and consistent method. Hint: Think about where you will find your results and how that location is affected by subsequent tests.  
 I will use both test cases provided as well as my own test cases to ensure that the functionality is performing as expected. To determine if the correct results are being saved, I will look at the RAM after each operation. The expected answers should be saved in their respective bytes of RAM.  Also, I will step through the program several times to make sure operations are being completed as planned.  

#####2. What should your program do if an operator is unknown?  
If the operator is unknown, I plan on exiting the program. I will possibly set use a specific register set with a certain bit to show that there was an unknown operator.  Another potential solution is having the program go into a CPU trap labeled along the lines of unknownOperator.  
Post-lab: I utilized a label called invalid_op.

#####3. Are there any bytes that you cannot use as an operand?  
0x55 cannot be used as it will trigger the end operation and the termination of the program.  
Post-lab: 0x44 will cause a similar result (except it will be a CLR operation).  ***These are only true if used as the very first operand.  If they are used as second operands later on, they will work as operands.***

#####4. Are there any other ways to break your program? For example, what would happen if someone were to provide a loooooonnnggg set of operations?
If someone provides an exceptionally long set of operations, there is the possibility that there will be not enough space in ROM or RAM to handle all of the operations and this would cause a failure in the program. Also, there is the possibility that certain unexpected instructions could send the program to terminate.  
Post-lab: I did not implement any features to circle back around or end the program in case of overflow.  Invalid operations do cause the program to terminate, but there may still be vulnerabilities in the input handling that I am unaware of.

#####5. Is there anything about this lab that you would like me to address at the beginning of class?
Could you go over using constants instead of magic numbers? I am not entirely sure how to use constants.

### Observations and Conclusions
This assignment took way longer than I expected, and I definitely should have started way sooner than I did.  Also, I learned that you have to be extra conscious of using the correct syntax with CCS/MSP430 Assembly because I had a lot of silent failures that required me to have to comb through the code to find the issues.  Furthermore,  I realized that something as seemingly simple as a calculator can become extremely complex.  I had to revise my flow chart and code several times after I realized that I was not checking for some particular case such as if CLR_OP is the first or second byte in the instructions.  Also, I believe I spent a lot of time trying to polish up the project, and I feel accomplished as a result of it working as I expect it to. I am not sure how long other cadets spent on this assignment, but I believe I spent around 10 hours from start to finish. This length might be due to me not being as prepared as I should have been or perhaps due to me constantly revising my flow chart/code.


### Feedback
Future students really need to be aware that getting a good product may require a significant amount of planning, debugging, and revising. It was stressed that planning was important, but I did not realize how important. Once I had a "good" flow chart, the coding became a lot less difficult.  It also allowed me to easily revise my design and have a visual to follow while I modified my code.

### Documentation
C2C Beresford pointed me in the direction of using Peasant Multiplication for the A Functionality.  He also helped explain how to correctly use .text, .data, and constants at the beginning of the program.
