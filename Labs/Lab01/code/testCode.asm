;-------------------------------------------------------------------------------
; Lab 1 - Assembly Language - "A Simple Calculator"
; C2C Christopher Do, USAF / Sept 2017
;
; This file is for testing code separately.
;-------------------------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file
            
;-------------------------------------------------------------------------------
            .def    RESET                   ; Export program entry-point to
                                            ; make it known to linker.
;-------------------------------------------------------------------------------
            .text                           ; Assemble into program memory.
            .retain                         ; Override ELF conditional linking
                                            ; and retain current section.
            .retainrefs                     ; And retain any sections that have
                                            ; references to current section.

myProgram:  .byte   0x44, 0x11, 0x11

;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer


;-------------------------------------------------------------------------------
; Main loop here
;-------------------------------------------------------------------------------
			.data
myResults:	.space 100

ADD_OP:		.equ	0x11
SUB_OP:		.equ	0x22
MUL_OP:		.equ	0x33
CLR_OP:		.equ	0x44
END_OP:		.equ	0x55

			.text

main:
			clr 	r4					; Pointer to calculator instructions
			clr		r5					; Operand 1 (or END_OP/CLR_OP)
			clr		r6					; Operation
			clr		r7					; Operand 2
			clr		r8					; Pointer to results
			clr		r9					; Temporary result register

			mov		#17,		r5
			mov		#13,		r6
			mov		#0,			r7
			mov		#1,			r8
			mov		#2,			r9

mul_op:
			clr		r9

mul_loop_1:
			bit.w	#0000000000000001,r5	; Test if the LSB is a 1
			jnc     mul_loop_2
			add		r6,			r9

mul_loop_2:
			cmp		#2,			r5
			jl		forever

			rra		r5
			rla		r6

			jmp		mul_loop_1


forever:	jmp		forever
                                            

;-------------------------------------------------------------------------------
; Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect   .stack
            
;-------------------------------------------------------------------------------
; Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET
            
