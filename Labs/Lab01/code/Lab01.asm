;-------------------------------------------------------------------------------
; Lab 1 - Assembly Language - "A Simple Calculator"
; C2C Christopher Do, USAF / Sept 2017
;
; This program provides a solution for the problem
; posed in Lab 1 for ECE 382 by implementing a
; calculator in assembly.  The calculator
; can add, subtract, and multiply. It also has
; functions to clear results and end operations.
;-------------------------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file
            
;-------------------------------------------------------------------------------
            .def    RESET                   ; Export program entry-point to
                                            ; make it known to linker.
;-------------------------------------------------------------------------------
            .text                           ; Assemble into program memory.
            .retain                         ; Override ELF conditional linking
                                            ; and retain current section.
            .retainrefs                     ; And retain any sections that have
                                            ; references to current section.

myProgram:  .byte   0x11, 0x44, 0x00, 0x11, 0x02, 0x33, 0x55, 0x11, 0x44, 0x55, 0x11, 0x11

;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer


;-------------------------------------------------------------------------------
; Main loop here
;-------------------------------------------------------------------------------
			.data
myResults:	.space 100

ADD_OP:		.equ	0x11
SUB_OP:		.equ	0x22
MUL_OP:		.equ	0x33
CLR_OP:		.equ	0x44
END_OP:		.equ	0x55

			.text

main:
			clr 	r4					; Pointer to calculator instructions
			clr		r5					; Operand 1 (or END_OP/CLR_OP)
			clr		r6					; Operation
			clr		r7					; Operand 2
			clr		r8					; Pointer to results
			clr		r9					; Temporary result register

			mov		#myProgram,	r4
			mov		#myResults, r8

			mov.b	@r4,		r5
			inc		r4

initial_end_check:
			cmp		#END_OP,	r5
			jeq		end_op

initial_clr_check:
			cmp		#CLR_OP,	r5
			jeq		clr_op

			mov.b 	@r4,		r6
			inc		r4

			cmp		#END_OP,	r6
			jeq		end_op

			cmp		#CLR_OP,	r6
			jeq		clr_op

			mov.b 	@r4,		r7
			inc		r4

op_check:
			cmp		#ADD_OP,	r6
			jeq		add_op

			cmp		#SUB_OP,	r6
			jeq		sub_op

			cmp		#MUL_OP,	r6
			jeq		mul_op

			jmp		invalid_op

load_bytes:
			mov.b 	@r4,		r6
			inc		r4

			cmp		#END_OP,	r6
			jeq		end_op

			cmp		#CLR_OP,	r6
			jeq		clr_op

			mov.b 	@r4,		r7
			inc		r4

			jmp		op_check

add_op:
			mov		r5,			r9
			add		r7,			r9
			jmp		check_result

sub_op:
			mov		r5,			r9
			sub		r7,			r9
			jmp		check_result

mul_op:
			clr		r9

mul_loop_1:
			bit.w	#0000000000000001,r5; Test if the LSB is a 1
			jnc     mul_loop_2
			add		r7,			r9

mul_loop_2:
			cmp		#2,			r5		; Is r5 a 0 or 1?
			jl		check_result

			rra		r5
			rla		r7

			jmp		mul_loop_1

check_result:
			cmp		#1,			r9		; Result <= 0?
			jl		result_to_0

			cmp		#255,		r9		; Result >= 255?
			jge		result_to_255

			jmp		store_result

result_to_0:
			mov		#0x00,		r9
			jmp		store_result

result_to_255:
			mov		#0xFF,		r9
			jmp		store_result

store_result:
			mov		r9,			r5
			mov.b	r9,			0(r8)
			inc		r8
			jmp		load_bytes

clr_op:
			mov		#0x00,		r9
			mov.b	r9,			0(r8)
			inc		r8

			mov.b	@r4,		r5
			inc		r4

			jmp		load_bytes


invalid_op:	jmp 	invalid_op
end_op:		jmp		end_op
                                            

;-------------------------------------------------------------------------------
; Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect   .stack
            
;-------------------------------------------------------------------------------
; Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET
            
