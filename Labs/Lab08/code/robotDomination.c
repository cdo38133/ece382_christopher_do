/**********************************************************************

 COPYRIGHT 2017 United States Air Force Academy All rights reserved.

 United States Air Force Academy     __  _______ ___    _________
 Dept of Electrical &               / / / / ___//   |  / ____/   |
 Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
 USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|

 ----------------------------------------------------------------------

   FILENAME      : robotDomination.c
   AUTHOR(S)     : Christopher Do
   DATE          : December 2017
   COURSE		 : ECE 382

   Lab 8

   DESCRIPTION   : Functionality for a robot that can dominate a simple
   maze and all other robots. Disclaimer: can't actually dominate anything.

   DOCUMENTATION : C2C Beresford and C2C Schwartz discussed pathfinding
   algorithms with me to find a balance of speed and ease of coding.

 Academic Integrity Statement: I certify that, while others may have
 assisted me in brain storming, debugging and validating this program,
 the program itself is my own work. I understand that submitting code
 which is the work of other individuals is a violation of the honor
 code.  I also understand that if I knowingly give my original work to
 another individual is also a violation of the honor code.

 **********************************************************************/

#include <msp430.h>
#include <stdint.h>	 // provides std data types (e.g., uint8_t)
#include <stdio.h>
#include <stdlib.h>
#include "ir.h"

#define SERVO_LEFT		0
#define	SERVO_MIDDLE	1
#define	SERVO_RIGHT		2

#define TRIGGER_DELAY	100
#define SERVO_DELAY		2500000
#define ECHO_DELAY		2000000
#define SIXTY_MS		60000
#define LED_DELAY		250000

#define SMALL_DELAY		1250000
#define MED_DELAY		2500000
#define BIG_DELAY		10000000

#define LEFT_DELAY		2550000
#define RIGHT_DELAY		2550000

#define THRESHOLD		2400
#define LEFT_THRESHOLD	2000

#define ECHO_PIN		(P1IN & BIT4)
#define	HIGH_2_LOW_ECHO	P1IES |= BIT4
#define	LOW_2_HIGH_ECHO	P1IES &= ~BIT4

volatile int16_t	pulseDuration = 0;
volatile uint8_t	servoPosition = SERVO_MIDDLE;
volatile int16_t	leftDistance = 0;
volatile int16_t	middleDistance = 0;
volatile int16_t	rightDistance = 0;

void sensorTrigger();

void pathSense();

void servoLeft();
void servoMiddle();
void servoRight();

void moveForward(void);
void moveBackward(void);
void smallLeft(void);
void bigLeft(void);
void smallRight(void);
void bigRight(void);
void stopMoving(void);

void checkForward();
void checkLeft();

void main(void)
{
	initMSP430();

	__delay_cycles(MED_DELAY);
	servoMiddle();

	while(1)
	{
		checkForward();

		if (middleDistance > THRESHOLD)
		{
			moveForward();
		}

		else
		{
			stopMoving();

			checkLeft();

			if (leftDistance > LEFT_THRESHOLD)
			{
				bigLeft();
			}
			else
			{
				bigRight();
			}
		}
	}
}


/************************************************************************************/
/* FUNCTION DECLARATIONS															*/
/************************************************************************************/

/**************************************************
   Function: initMSP430()
   Author: Christopher Do
   Description: Initializes the MSP430
   Inputs: None
   Returns: None
***************************************************/
void initMSP430() {
		WDTCTL = WDTPW|WDTHOLD;			// stop the watchdog timer

	    BCSCTL1 = CALBC1_8MHZ;
	    DCOCTL = CALDCO_8MHZ;

	    P1DIR |=  BIT0;					// LEDs
	    P1OUT &= ~BIT0;
	    P1DIR |=  BIT6;
	    P1OUT &= ~BIT6;

	    P1DIR |=  BIT3;					// Trigger
	    P1SEL &= ~BIT3;
	    P1SEL2 &= ~BIT3;
	    P1OUT &= ~BIT3;

		P1DIR &= ~BIT4;					// Echo input
		P1SEL &= ~BIT4;
		P1SEL2 &= ~BIT4;
		P1OUT &= ~BIT4;

		P1DIR |=  BIT2;					// Servo PWM
		P1SEL &= ~BIT2;
		P1OUT &= ~BIT2;

		P2DIR |=  BIT1;					// TA1CCR1 on P2.1
		P2SEL &= ~BIT1;					// GPIO by default for safety
		P2OUT &= ~BIT1;

		P2DIR |=  BIT2;					// TA1CCR1 on P2.2
		P2SEL &= ~BIT2;
		P2OUT &= ~BIT2;

		P2DIR |=  BIT4;					// TA1CCR2 on P2.4
		P2SEL &= ~BIT4;
		P2OUT &= ~BIT4;

		P2DIR |=  BIT5;					// TA1CCR2 on P2.5
		P2SEL &= ~BIT5;
		P2OUT &= ~BIT5;

		TA1CTL |= TASSEL_2|MC_1|ID_0;	// configure for SMCLK

		TA1CCR0 = 1000;					// set signal period to 1000 clock cycles (~1 millisecond)
		TA1CCR1 = 510;					// set duty cycle to 0/1000
		TA1CCR2 = 500;					// set duty cycle to 0/1000
		TA1CCTL1 |= OUTMOD_7;			// set TACCTL1 to Reset / Set mode
		TA1CCTL2 |= OUTMOD_7;			// set TACCTL2 to Reset / Set mode

	    TA0CTL |= TASSEL_2|MC_1|ID_3;

	    TA0CCR0 = 20000;
	    TA0CCR1 = 1500;
	    TA0CCTL1 |= OUTMOD_7;

		P1IFG &= ~BIT4;					// Clear any interrupt flag on P1.4
		P1IE  |= BIT4;					// Enable P1.4 interrupt

		HIGH_2_LOW_ECHO;				// check the header out.  P1IES changed.

		TA0R = 0;						// ensure TAR is clear
		TA0CTL &= ~TAIFG;				// clear flag before enabling interrupts = good practice

		__enable_interrupt();
}

/**************************************************
   Function: sensorTrigger()
   Author: Christopher Do
   Description: Send a pulse to trigger the ultra-
   sonic sensor to determine if there is an object.
   Inputs: None
   Returns: None
***************************************************/
void sensorTrigger()
{
	pulseDuration = 0;

	P1OUT |=  BIT3;
	__delay_cycles(TRIGGER_DELAY);
	P1OUT &= ~BIT3;

	while (pulseDuration == 0);

	if (pulseDuration == -1) // If the timer over flowed
	{
		pulseDuration = THRESHOLD + 1;
	}
}

/**************************************************
   Function: pathSense()
   Author: Christopher Do
   Description: Where are we going?
   Inputs: None
   Returns: None
***************************************************/
void pathSense()
{
	leftDistance = 0;
	rightDistance = 0;
	middleDistance = 0;

	servoLeft();
	__delay_cycles(SERVO_DELAY);
	sensorTrigger();
	__delay_cycles(ECHO_DELAY);

	if (servoPosition == SERVO_LEFT)
	{
		leftDistance = pulseDuration;

		if (leftDistance < THRESHOLD)
		{
			P1OUT |=  BIT0;

			__delay_cycles(LED_DELAY);

			P1OUT &= ~BIT0;
		}
	}

	servoRight();
	__delay_cycles(SERVO_DELAY);
	sensorTrigger();
	__delay_cycles(ECHO_DELAY);

	if (servoPosition == SERVO_RIGHT)
	{
		rightDistance = pulseDuration;

		if (rightDistance < THRESHOLD)
		{
			P1OUT |=  BIT6;

			__delay_cycles(LED_DELAY);

			P1OUT &= ~BIT6;
		}
	}

	servoMiddle();
	__delay_cycles(SERVO_DELAY);
	sensorTrigger();
	__delay_cycles(ECHO_DELAY);

	if (servoPosition == SERVO_MIDDLE)
	{
		middleDistance = pulseDuration;

		if (middleDistance < THRESHOLD)
		{
			P1OUT |=  BIT0;
			P1OUT |=  BIT6;

			__delay_cycles(LED_DELAY);

			P1OUT &= ~BIT0;
			P1OUT &= ~BIT6;
		}
	}
}

/**************************************************
   Function: checkForward()
   Author: Christopher Do
   Description: Can I move forward?
   Inputs: None
   Returns: None
***************************************************/
void checkForward()
{
	middleDistance = 0;

	if (servoPosition != SERVO_MIDDLE)
	{
		servoMiddle();
		__delay_cycles(SERVO_DELAY);
	}

	sensorTrigger();
	__delay_cycles(ECHO_DELAY);

	middleDistance = pulseDuration;
}

/**************************************************
   Function: checkLeft()
   Author: Christopher Do
   Description: Can I move forward?
   Inputs: None
   Returns: None
***************************************************/
void checkLeft()
{
	leftDistance = 0;

	if (servoPosition != SERVO_LEFT)
	{
		servoLeft();
		__delay_cycles(SERVO_DELAY);
	}

	sensorTrigger();
	__delay_cycles(ECHO_DELAY);

	leftDistance = pulseDuration;
}

/**************************************************
   Function: stopMoving()
   Author: Christopher Do
   Description: Stop the robot
   Inputs: None
   Returns: None
***************************************************/
void stopMoving(void)
{
	P2SEL &= ~BIT1;
	P2SEL &= ~BIT2;
	P2SEL &= ~BIT4;
	P2SEL &= ~BIT5;

	__delay_cycles(MED_DELAY);
}

/**************************************************
   Function: moveForward()
   Author: Christopher Do
   Description: Moves the robot forwards
   Inputs: None
   Returns: None
***************************************************/
void moveForward(void)
{
	P2SEL |=  BIT1;
	P2SEL &= ~BIT2;
	P2SEL |=  BIT4;
	P2SEL &= ~BIT5;
}

/**************************************************
   Function: moveBackward()
   Author: Christopher Do
   Description: Moves the robot backwards
   Inputs: None
   Returns: None
***************************************************/
void moveBackward(void)
{
	P2SEL &= ~BIT1;
	P2SEL |=  BIT2;
	P2SEL &= ~BIT4;
	P2SEL |=  BIT5;
}

/**************************************************
   Function: smallLeft()
   Author: Christopher Do
   Description: Turn the robot to the left fewer
   than 45 degrees
   Inputs: None
   Returns: None
***************************************************/
void smallLeft(void)
{
	P2SEL &= ~BIT1;
	P2SEL |=  BIT2;
	P2SEL |=  BIT4;
	P2SEL &= ~BIT5;

	__delay_cycles(SMALL_DELAY);

	stopMoving();
}

/**************************************************
   Function: bigLeft()
   Author: Christopher Do
   Description: Turn the robot to the left greater
   than 45 degrees
   Inputs: None
   Returns: None
***************************************************/
void bigLeft(void)
{
	P2SEL &= ~BIT1;
	P2SEL |=  BIT2;
	P2SEL |=  BIT4;
	P2SEL &= ~BIT5;

	__delay_cycles(LEFT_DELAY);

	stopMoving();
}

/**************************************************
   Function: smallRight()
   Author: Christopher Do
   Description: Turn the robot to the right fewer
   than 45 degrees
   Inputs: None
   Returns: None
***************************************************/
void smallRight(void)
{
	P2SEL |=  BIT1;
	P2SEL &= ~BIT2;
	P2SEL &= ~BIT4;
	P2SEL |=  BIT5;

	__delay_cycles(SMALL_DELAY);

	stopMoving();
}

/**************************************************
   Function: bigRight()
   Author: Christopher Do
   Description: Turn the robot to the right greater
   than 45 degrees
   Inputs: None
   Returns: None
***************************************************/
void bigRight(void)
{
	P2SEL |=  BIT1;
	P2SEL &= ~BIT2;
	P2SEL &= ~BIT4;
	P2SEL |=  BIT5;

	__delay_cycles(RIGHT_DELAY);

	stopMoving();
}

/**************************************************
   Function: servoLeft()
   Author: Christopher Do
   Description: Moves the servo towards the left
   Inputs: None
   Returns: None
***************************************************/
void servoLeft()
{
	TA0CCR1 = 2550;

	P1SEL |=  BIT2;
	__delay_cycles(SERVO_DELAY);
	P1SEL &= ~BIT2;

	servoPosition = SERVO_LEFT;
}

/**************************************************
   Function: servoMiddle()
   Author: Christopher Do
   Description: Moves the servo towards the middle
   Inputs: None
   Returns: None
***************************************************/
void servoMiddle()
{
	TA0CCR1 = 1500;

	P1SEL |=  BIT2;
	__delay_cycles(SERVO_DELAY);
	P1SEL &= ~BIT2;

	servoPosition = SERVO_MIDDLE;
}

/**************************************************
   Function: servoRight()
   Author: Christopher Do
   Description: Moves the servo towards the right
   Inputs: None
   Returns: None
***************************************************/
void servoRight()
{
	TA0CCR1 = 400;

	P1SEL |=  BIT2;
	__delay_cycles(SERVO_DELAY);
	P1SEL &= ~BIT2;

	servoPosition = SERVO_RIGHT;
}

// -----------------------------------------------------------------------
// Interrupts
// -----------------------------------------------------------------------

#pragma vector = PORT1_VECTOR

__interrupt void pinChangeEcho (void) {
	int8	echo_pin;

	if(ECHO_PIN) echo_pin = 1; else echo_pin = 0;

	switch (echo_pin) {
		case 0:
			pulseDuration = TA0R;
			TA0CTL &= ~(TAIE);
			LOW_2_HIGH_ECHO; 						// Set up pin interrupt on positive edge
			break;

		case 1:
			TA0CTL &= ~TAIFG;
			TA0CTL |= TAIE; 					// Enable timer A
			TA0R = 0x0000;						// time measurements are based at time 0
			HIGH_2_LOW_ECHO; 						// Set up pin interrupt on falling edge
			break;
	}	// end switch
	P1IFG &= ~BIT4;								// Clear the interrupt flag
} // end pinChange ISR

#pragma vector = TIMER0_A1_VECTOR
__interrupt void timerOverflowEcho (void) {
	TA0CTL &= ~TAIE;
	TA0CTL &= ~TAIFG;
	P1IFG &= ~BIT4;
	pulseDuration = -1;
} // end TimerA ISR
