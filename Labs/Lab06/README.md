# Lab 6 - PWM - "Robot Motion"

## By C2C Christopher Do

## Table of Contents
1. [Prelab](#markdown-prelab)
2. [Documentation](#markdown-header-documentation)

### Prelab
1. Which pins will output which signals you need?
	1. TA0.0 on pin 3
	2. TA0.1 on pin 4
	2. TA1.0 on pin 8
	3. TA1.1 on pin 9
2. Which side of the motor will you attach these signals to?
	1. Pin 3 to the positive terminal of the left motor
	2. Pin 4 to the negative terminal of the left motor
	3. Pin 8 to the positive terminal of the right motor
	4. Pin 9 to the negative terminal of the right motor
3. How will you use these signals to achieve forward / back / left / right movement?
	1. To go forward, both of the positive terminals will be set as high and the negative terminals will be set as low.
	2. To go backwards, the opposite of forward.
	3. To turn left, the positive of the left motor low with negative high; the positive of the right motor high with negative low.
	4. To turn right, the opposite of left.
4. What are the registers you'll need to use?  
See Below
5. Which bits in those registers are important?
	1. Capture/Compare Control Register
		1. Capture mode (15-14)
		2. CC Input Select (13-12)
		3. Sync capture source (11)
		4. CC Interupt Flag (0)
	2. Time_A Control Register
		1. Clock Source Select (9-8)
		2. Input Divider (7-6)
		3. Mode control (5-4)
		4. Clear (2)
		5. Interrupt Enable (1)
		6. Interrupt Flag (0)
6. What's the initialization sequence you'll need?
	1. Set I/O direction for output
	2. MUX PWM
	3. Set period
	4. Set on time
	5. Enable PWM mode on timer
	6. Config/Start timer

![imgTemplate](images/imgTemplate.png)
##### Figure 1: imgTemplate.


### Documentation
None
