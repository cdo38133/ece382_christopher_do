#include <msp430.h>

#define DELAY_CYCLES 500000

void main(void)
{
    WDTCTL = WDTPW|WDTHOLD;                 // stop the watchdog timer

    BCSCTL1 = CALBC1_8MHZ;
    DCOCTL = CALDCO_8MHZ;

    P2DIR |= BIT1;                // TA1CCR1 on P2.1
    P2SEL |= BIT1;
    P2OUT &= ~BIT1;

    P2DIR |= BIT2;                // TA1CCR1 on P2.2
    P2SEL |= BIT2;
    P2OUT &= ~BIT2;

    P2DIR |= BIT4;                // TA1CCR2 on P2.4
    P2SEL |= BIT4;
    P2OUT &= ~BIT4;

    P2DIR |= BIT5;                // TA1CCR2 on P2.5
    P2SEL |= BIT5;
    P2OUT &= ~BIT5;

    TA1CTL |= TASSEL_2|MC_1|ID_0;           // configure for SMCLK

    //use LED (P1.0) to indicate duty cycle has toggled and button (P1.3) to toggle
    P1DIR = BIT0;
    P1REN = BIT3;
    P1OUT = BIT3;

    TA1CCR0 = 1000;                // set signal period to 1000 clock cycles (~1 millisecond)
    TA1CCR1 = 250;                // set duty cycle to 250/1000 (75% for set/reset)
    TA1CCR2 = 500;                // set duty cycle to 250/1000 (75% for set/reset)
    TA1CCTL1 |= OUTMOD_3;        // set TACCTL1 to Set / Reset mode
    TA1CCTL2 |= OUTMOD_3;        // set TACCTL2 to Set / Reset mode


    while (1) {

        while (P1IN & BIT3);    //every time the button is pushed, toggle the duty cycle
        __delay_cycles(DELAY_CYCLES);
        TA1CTL &= ~MC_1;
        TA1CCR1 = 500;            // set duty cycle to 500/1000 (50%)
        TA1CTL |= MC_1;
        P1OUT ^= BIT0;

        P2SEL |= BIT4;
        P2SEL &= ~BIT5;

        while (P1IN & BIT3);
        __delay_cycles(DELAY_CYCLES);
        TA1CTL &= ~MC_1;
        TA1CCR1 = 750;            // set duty cycle to 750/1000 (25%)
        TA1CTL |= MC_1;
        P1OUT ^= BIT0;

        P2SEL &= ~BIT4;
        P2SEL |= BIT5;

        while (P1IN & BIT3);
        __delay_cycles(DELAY_CYCLES);
        TA1CTL &= ~MC_1;
        TA1CCR1 = 1000;            // set duty cycle to 1000/1000 (0%)
        TA1CTL |= MC_1;
        P1OUT ^= BIT0;

        P2SEL |= BIT4;
        P2SEL &= ~BIT5;

        while (P1IN & BIT3);
        __delay_cycles(DELAY_CYCLES);
        TA1CTL &= ~MC_1;
        TA1CCR1 = 0;            // set duty cycle to 0/1000 (100%)
        TA1CTL |= MC_1;
        P1OUT ^= BIT0;

        P2SEL &= ~BIT4;
        P2SEL |= BIT5;

        while (P1IN & BIT3);
        __delay_cycles(DELAY_CYCLES);
        TA1CTL &= ~MC_1;
        TA1CCR1 = 100;            // set duty cycle to 100/1000 (90%)
        TA1CTL |= MC_1;
        P1OUT ^= BIT0;

        P2SEL |= BIT4;
        P2SEL &= ~BIT5;

        while (P1IN & BIT3);
        __delay_cycles(DELAY_CYCLES);
        TA1CTL &= ~MC_1;
        TA1CCR1 = 250;            // set duty cycle to 250/1000 (75% for set/reset)
        TA1CTL |= MC_1;
        P1OUT ^= BIT0;

        P2SEL &= ~BIT4;
        P2SEL |= BIT5;
    }
}
