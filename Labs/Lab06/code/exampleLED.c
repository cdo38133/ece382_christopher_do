#include <msp430.h>

#define DELAY_CYCLES 500000

void main(void)
{
        WDTCTL = WDTPW|WDTHOLD;                 // stop the watchdog timer

        // I/O setup
        P1DIR |= BIT0;                //use LED to indicate duty cycle has toggled
        P1REN |= BIT3;                // Button S2 used to change duty cycle of PWM
        P1OUT |= BIT3;

        // TA1CCR1 on P2.1
        P2DIR |= BIT1;
        P2SEL |= BIT1;
        P2OUT &= ~BIT1;

        TA1CTL |= TASSEL_2|MC_1|ID_0; // configure Timer 1 (SMCLK, UP mode)

        // PWM setup
        TA1CCR0 = 1000;               // set signal period to 1000 clock cycles (~1 millisecond)
        TA1CCTL0 |= CCIE;            // enable CC interrupts

        TA1CCR1 = 250;               // set duty cycle to 250/1000 (25%)
        TA1CCTL1 |= OUTMOD_7|CCIE;  // set TACCTL1 to Reset / Set mode//enable CC interrupts

        //clear capture compare interrupt flags
        TA1CCTL0 &= ~CCIFG;
        TA1CCTL1 &= ~CCIFG;

        _enable_interrupt();

        while (1) {

            while (P1IN & BIT3);     //every time the button is pushed, toggle the duty cycle
            __delay_cycles(DELAY_CYCLES);
            TA1CTL &= ~MC_1;
            TA1CCR1 = 1000;            // set duty cycle to 1000/1000 (100%)
            TA1CTL |= MC_1;

            while (P1IN & BIT3);
            __delay_cycles(DELAY_CYCLES);
            TA1CTL &= ~MC_1;
            TA1CCR1 = 750;            // set duty cycle to 750/1000 (75%)
            TA1CTL |= MC_1;

            while (P1IN & BIT3);
            __delay_cycles(DELAY_CYCLES);
            TA1CTL &= ~MC_1;
            TA1CCR1 = 500;            // set duty cycle to 500/1000 (50%)
            TA1CTL |= MC_1;

            while (P1IN & BIT3);
            __delay_cycles(DELAY_CYCLES);
            TA1CTL &= ~MC_1;
            TA1CCR1 = 250;            // set duty cycle to 250/1000 (25%)
            TA1CTL |= MC_1;

            while (P1IN & BIT3);
            __delay_cycles(DELAY_CYCLES);
            TA1CTL &= ~MC_1;
            TA1CCR1 = 100;            // set duty cycle to 100/1000 (10%)
            TA1CTL |= MC_1;

            while (P1IN & BIT3);
            __delay_cycles(DELAY_CYCLES);
            TA1CTL &= ~MC_1;
            TA1CCR1 = 20;            // set duty cycle to 20/1000 (2%)
            TA1CTL |= MC_1;

        }
}


#pragma vector = TIMER1_A0_VECTOR            // TA1CCR0 CCIFG vector
__interrupt void TA1captureCompare0_ISR (void) {
    P1OUT |= BIT0;                        //Turn on LED
    TA1CCTL1 &= ~CCIFG;                  //clear capture compare interrupt flag
}

#pragma vector = TIMER1_A1_VECTOR            // TA1 CCR2 and CCR1 CCIFG, TAIFG vector
__interrupt void TA1captureCompare1_ISR (void) {
    P1OUT &= ~BIT0;                        //Turn off LED
    TA1CCTL1 &= ~CCIFG;                   //clear capture compare interrupt flag

    /* You might have other interrupt sources to deal with as well
       TAIV tells you which specific interrupt flags are set

       if(TA1IV == TA1IV_TAIFG)
       {
         do stuff for TA overflow
         TA1CTL &= ~TAIFG;
       }
    */
}
