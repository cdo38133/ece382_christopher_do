/**********************************************************************

 COPYRIGHT 2017 United States Air Force Academy All rights reserved.

 United States Air Force Academy     __  _______ ___    _________
 Dept of Electrical &               / / / / ___//   |  / ____/   |
 Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
 USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|

 ----------------------------------------------------------------------

   FILENAME      : robotMotion.c
   AUTHOR(S)     : Christopher Do
   DATE          : November 2017
   COURSE		 : ECE 382

   Lab 6

   DESCRIPTION   : Functionality for moving a robot.

   DOCUMENTATION : C2C Darcy helped significantly with my IR code.
   I had Lab 5 working, but copying and pasting the code and altering
   did not work as expected.  He went with me through the code to help
   debug and fix errors.  He only helped with IR functionality.

 Academic Integrity Statement: I certify that, while others may have
 assisted me in brain storming, debugging and validating this program,
 the program itself is my own work. I understand that submitting code
 which is the work of other individuals is a violation of the honor
 code.  I also understand that if I knowingly give my original work to
 another individual is also a violation of the honor code.

 **********************************************************************/

#include <msp430.h>
#include <stdint.h>	 // provides std data types (e.g., uint8_t)
#include <stdio.h>
#include <stdlib.h>
#include "ir.h"

volatile uint32_t	irPacket = 0;
volatile uint32_t	lastirPacket = 0;
volatile uint8_t	numRepeats = 0;
volatile uint16_t	packetData[32];
volatile uint8_t	newIrPacket = FALSE;
volatile uint8_t	packetIndex = 0;

#define SMALL_DELAY	   1250000
#define MED_DELAY	   2500000
#define BIG_DELAY	   10000000

void moveForward(void);
void moveBackward(void);
void smallLeft(void);
void bigLeft(void);
void smallRight(void);
void bigRight(void);

void main(void)
{
	initMSP430();

	moveForward();
	__delay_cycles(BIG_DELAY);

	moveBackward();
	__delay_cycles(BIG_DELAY);

	smallLeft();
	__delay_cycles(BIG_DELAY);

	smallRight();
	__delay_cycles(BIG_DELAY);

	bigLeft();
	__delay_cycles(BIG_DELAY);

	bigRight();
	__delay_cycles(BIG_DELAY);

	while(1)  {
		if (newIrPacket == TRUE) {
			newIrPacket = FALSE;
			if (irPacket == UP) {
				moveForward();
			} else if (irPacket == DOWN){
				moveBackward();
			} else if (irPacket == RIGHT) {
				smallRight();
			} else if (irPacket == LEFT) {
				smallLeft();
			}
			irPacket = 0;
		}
	}
}


/************************************************************************************/
/* FUNCTION DECLARATIONS															*/
/************************************************************************************/

/**************************************************
   Function: initMSP430()
   Author: Christopher Do
   Description: Initializes the MSP430
   Inputs: None
   Returns: None
***************************************************/
void initMSP430() {
		WDTCTL = WDTPW|WDTHOLD;			// stop the watchdog timer

	    BCSCTL1 = CALBC1_8MHZ;
	    DCOCTL = CALDCO_8MHZ;

	    P1DIR = BIT0;
	    P1REN = BIT3;
	    P1OUT = BIT3;

	    P2DIR |=  BIT1;					// TA1CCR1 on P2.1
	    P2SEL &= ~BIT1;					// GPIO by default for safety
	    P2OUT &= ~BIT1;

	    P2DIR |=  BIT2;					// TA1CCR1 on P2.2
	    P2SEL &= ~BIT2;
	    P2OUT &= ~BIT2;

	    P2DIR |=  BIT4;					// TA1CCR2 on P2.4
	    P2SEL &= ~BIT4;
	    P2OUT &= ~BIT4;

	    P2DIR |=  BIT5;					// TA1CCR2 on P2.5
	    P2SEL &= ~BIT5;
	    P2OUT &= ~BIT5;

	    TA1CTL |= TASSEL_2|MC_1|ID_0;	// configure for SMCLK

	    TA1CCR0 = 1000;					// set signal period to 1000 clock cycles (~1 millisecond)
	    TA1CCR1 = 500;					// set duty cycle to 0/1000
	    TA1CCR2 = 500;					// set duty cycle to 0/1000
	    TA1CCTL1 |= OUTMOD_7;			// set TACCTL1 to Reset / Set mode
	    TA1CCTL2 |= OUTMOD_7;			// set TACCTL2 to Reset / Set mode

		P2DIR &= ~BIT6;					// Set up P2.6 as GPIO not XIN
		P2SEL &= ~BIT6;					// Once again, this take three lines
		P2SEL2 &= ~BIT6;				// to properly do

		P2IFG &= ~BIT6;					// Clear any interrupt flag on P2.6
		P2IE  |= BIT6;					// Enable P2.6 interrupt

		HIGH_2_LOW;						// check the header out.  P2IES changed.

		TA0R = 0;						// ensure TAR is clear
		TA0CCR0 = 16000 - 1;			// create a 16ms roll-over period
		TA0CTL &= ~TAIFG;				// clear flag before enabling interrupts = good practice
		TA0CTL = ID_3 | TASSEL_2;		// Use 1:8 prescalar off SMCLK and enable interrupts

		__enable_interrupt();
}

/**************************************************
   Function: moveForward()
   Author: Christopher Do
   Description: Moves the robot forwards
   Inputs: None
   Returns: None
***************************************************/
void moveForward(void)
{
	P2SEL |=  BIT1;
	P2SEL &= ~BIT2;
	P2SEL |=  BIT4;
	P2SEL &= ~BIT5;

	__delay_cycles(BIG_DELAY);

	P2SEL &= ~BIT1;
	P2SEL &= ~BIT2;
	P2SEL &= ~BIT4;
	P2SEL &= ~BIT5;
}

/**************************************************
   Function: moveBackward()
   Author: Christopher Do
   Description: Moves the robot backwards
   Inputs: None
   Returns: None
***************************************************/
void moveBackward(void)
{
	P2SEL &= ~BIT1;
	P2SEL |=  BIT2;
	P2SEL &= ~BIT4;
	P2SEL |=  BIT5;

	__delay_cycles(BIG_DELAY);

	P2SEL &= ~BIT1;
	P2SEL &= ~BIT2;
	P2SEL &= ~BIT4;
	P2SEL &= ~BIT5;
}


/**************************************************
   Function: smallLeft()
   Author: Christopher Do
   Description: Turn the robot to the left fewer
   than 45 degrees
   Inputs: None
   Returns: None
***************************************************/
void smallLeft(void)
{
	P2SEL &= ~BIT1;
	P2SEL |=  BIT2;
	P2SEL |=  BIT4;
	P2SEL &= ~BIT5;

	__delay_cycles(SMALL_DELAY);

	P2SEL &= ~BIT1;
	P2SEL &= ~BIT2;
	P2SEL &= ~BIT4;
	P2SEL &= ~BIT5;
}


/**************************************************
   Function: bigLeft()
   Author: Christopher Do
   Description: Turn the robot to the left greater
   than 45 degrees
   Inputs: None
   Returns: None
***************************************************/
void bigLeft(void)
{
	P2SEL &= ~BIT1;
	P2SEL |=  BIT2;
	P2SEL |=  BIT4;
	P2SEL &= ~BIT5;

	__delay_cycles(MED_DELAY);

	P2SEL &= ~BIT1;
	P2SEL &= ~BIT2;
	P2SEL &= ~BIT4;
	P2SEL &= ~BIT5;
}


/**************************************************
   Function: smallRight()
   Author: Christopher Do
   Description: Turn the robot to the right fewer
   than 45 degrees
   Inputs: None
   Returns: None
***************************************************/
void smallRight(void)
{
	P2SEL |=  BIT1;
	P2SEL &= ~BIT2;
	P2SEL &= ~BIT4;
	P2SEL |=  BIT5;

	__delay_cycles(SMALL_DELAY);

	P2SEL &= ~BIT1;
	P2SEL &= ~BIT2;
	P2SEL &= ~BIT4;
	P2SEL &= ~BIT5;
}


/**************************************************
   Function: bigRight()
   Author: Christopher Do
   Description: Turn the robot to the right greater
   than 45 degrees
   Inputs: None
   Returns: None
***************************************************/
void bigRight(void)
{
	P2SEL |=  BIT1;
	P2SEL &= ~BIT2;
	P2SEL &= ~BIT4;
	P2SEL |=  BIT5;

	__delay_cycles(MED_DELAY);

	P2SEL &= ~BIT1;
	P2SEL &= ~BIT2;
	P2SEL &= ~BIT4;
	P2SEL &= ~BIT5;
}

// -----------------------------------------------------------------------
// Since the IR decoder is connected to P2.6, we want an interrupt
// to occur every time that the pin changes - this will occur on
// a positive edge and a negative edge.
//
// Negative Edge:
// The negative edge is associated with end of the logic 1 half-bit and
// the start of the logic 0 half of the bit.  The timer contains the
// duration of the logic 1 pulse, so we'll pull that out, process it
// and store the bit in the global irPacket variable. Going forward there
// is really nothing interesting that happens in this period, because all
// the logic 0 half-bits have the same period.  So we will turn off
// the timer interrupts and wait for the next (positive) edge on P2.6
//
// Positive Edge:
// The positive edge is associated with the end of the logic 0 half-bit
// and the start of the logic 1 half-bit.  There is nothing to do in
// terms of the logic 0 half bit because it does not encode any useful
// information.  On the other hand, we going into the logic 1 half of the bit
// and the portion which determines the bit value, the start of the
// packet, or if the timer rolls over, the end of the ir packet.
// Since the duration of this half-bit determines the outcome
// we will turn on the timer and its associated interrupt.
// -----------------------------------------------------------------------
#pragma vector = PORT2_VECTOR			// This is from the MSP430G2553.h file

__interrupt void pinChange (void) {
	int16	pulseDuration;				// The timer is 16-bits
	int8	ir_pin;

	if(IR_PIN) ir_pin = 1; else ir_pin = 0;

	switch (ir_pin) {
		case 0:
		// !!!!!!!!!NEGATIVE EDGE!!!!!!!!!!
			pulseDuration = TA0R;			//**Note** If you don't specify TA1 or TA0 then TAR defaults to TA0R

			if (pulseDuration > minLogic0Pulse && pulseDuration < maxLogic0Pulse){			// Logic 0
				packetData[packetIndex] = pulseDuration;
				irPacket = irPacket << 1;
				packetIndex += 1;
			}
			else if (pulseDuration > minLogic1Pulse && pulseDuration < maxLogic1Pulse){		// Logic 1
				packetData[packetIndex] = pulseDuration;
				irPacket = irPacket << 1;
				irPacket += 1;
				packetIndex += 1;
			}

			TA0CTL &= ~(MC_1|TAIE); 					// Disable timer A
			LOW_2_HIGH; 						// Set up pin interrupt on positive edge
			break;

		case 1:
		// !!!!!!!!POSITIVE EDGE!!!!!!!!!!!
			TA0R = 0x0000;						// time measurements are based at time 0
			TA0CTL |= MC_1|TAIE; 					// Enable timer A
			HIGH_2_LOW; 						// Set up pin interrupt on falling edge
			break;
	}	// end switch

	P2IFG &= ~BIT6;							// Clear the interrupt flag to prevent immediate ISR re-entry

} // end pinChange ISR

#pragma vector = TIMER0_A1_VECTOR			// This is from the MSP430G2553.h file
__interrupt void timerOverflow (void) {
	TA0CTL &= ~(MC_1|TAIE); 						// Disable timer A
	newIrPacket = TRUE;
	packetIndex = 0;
	TA0CTL &= ~TAIFG;
} // end TimerA ISR
